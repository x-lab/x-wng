#include <QtCore>
#include <QtQml>
#include <QtWebView>

int main(int argc, char *argv[])
{
    if (argc != 2) {
        qDebug() << "Not this way";
        return 0;
    }

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);

    QtWebView::initialize();

    QGuiApplication application(argc, argv);

    QQmlApplicationEngine engine(QString("main.%1.qml").arg(argv[1]));

    return application.exec();
}
