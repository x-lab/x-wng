import * as d3 from 'd3';

export class DDDS {

    svg: any;

     width: number;
    height: number;

    constructor(width: number, height: number) {

        this.width = width;
        this.height = height;

        this.svg = d3.select('body')
            .append('svg')
            .attr('viewBox', [0, 0, width, height])
            .style('font', '10px sans-serif');
    }
}
