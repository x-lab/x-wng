import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Window   2.15
import QtWebEngine      1.10

ApplicationWindow {

    id: window;

    width: 860;
    height: 640;
    visible: true;

    background: Grid {

      id: _bg;

      property int size: 20;

         rows: window.height / _bg.size;
      columns: window.width  / _bg.size + 1;

      Repeater {


        model: _bg.rows * _bg.columns;

        Rectangle {
           width: _bg.size;
          height: _bg.size;
          color: (model.index % 2 == 0) ? "#F6F6F5" : "#E3D2E2";
        }
      }
    }

    WebEngineView {

        id: view;

        anchors.fill: parent;

        backgroundColor: "transparent";

        settings.javascriptEnabled: true
        settings.pluginsEnabled: true
        settings.autoLoadImages: true
        settings.showScrollBars: false;

        property var i: 0;
        property var n: 480;

        url: 'ect.html';

        onLoadingChanged: {
            switch(loadRequest.status) {
            case WebEngineLoadRequest.LoadStartedStatus: break;
            case WebEngineLoadRequest.LoadFailedStatus: break;
            case WebEngineLoadRequest.LoadSucceededStatus:
                view.runJavaScript("var instance = X.createECT6();");
                break;
            }
        }

        onJavaScriptConsoleMessage: {
            console.log(message, lineNumber, sourceID);
        }
    }
}
