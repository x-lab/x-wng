import { DDD1 } from './ddd1';
import { DDD2 } from './ddd2';
import { DDD3 } from './ddd3';
import { DDD4 } from './ddd4';

import { ECT1 } from './ect1';
import { ECT2 } from './ect2';
import { ECT3 } from './ect3';
import { ECT4 } from './ect4';
import { ECT5 } from './ect5';
import { ECT6 } from './ect6';
import { ECT7 } from './ect7';

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

export function createDDD1(width: number, height: number): DDD1
{
    return new DDD1(width, height);
}

export function createDDD2(width: number, height: number, data: any): DDD2
{
    return new DDD2(width, height, data);
}

export function createDDD3(width: number, height: number, data: any): DDD3
{
    return new DDD3(width, height, data);
}

export function createDDD4(width: number, height: number, data: any): DDD4
{
    return new DDD4(width, height, data);
}

// /////////////////////////////////////////////////////////////////////////////

export function createECT1(): ECT1 { return new ECT1(); }
export function createECT2(): ECT2 { return new ECT2(); }
export function createECT3(): ECT3 { return new ECT3(); }
export function createECT4(): ECT4 { return new ECT4(); }
export function createECT5(): ECT5 { return new ECT5(); }
export function createECT6(): ECT6 { return new ECT6(); }
export function createECT7(): ECT7 { return new ECT7(); }
