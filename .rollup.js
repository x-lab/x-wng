import resolve from '@rollup/plugin-node-resolve';
import typescript from '@rollup/plugin-typescript';
import json from '@rollup/plugin-json';
import injectProcessEnv from 'rollup-plugin-inject-process-env';

export default {
    input: 'main.ts',
    output: {
        file: 'main.js',
        format: 'iife',
        name: 'X',
    },
    plugins: [
        json(),
        resolve({
            jsnext: true
        }),
        injectProcessEnv({
            NODE_ENV: 'production',
            UNUSED: null
        }),
        typescript({
            tsconfig: false,
            allowSyntheticDefaultImports: true
        })
    ]
};
