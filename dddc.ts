import * as d3 from 'd3';

export class DDDC {

    canvas: any;
    context: any;

     width: number;
    height: number;

    constructor(width: number, height: number) {

        this.width = width;
        this.height = this.width / 1.8;

        this.canvas = d3.select('body')
            .append('canvas')
            .attr('width', width)
            .attr('height', height);

        this.context = this.canvas.node().getContext('2d');
    }
}
