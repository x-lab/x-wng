### Requirements

- `node`
- `npm`
- `qt > 5.15` # No conda install, currently under investigation

### Development process

** (0) - Get the skeleton ready

- [package.json]
  - `devDependencies`: rollup group
- [.rollup.js]
  - `input` set
  - `output` set

** (1) - Declare npm deps

``` shell
npm install --save [dep1]
npm install --save [dep2]
npm install --save d3
```

** (2) - Install npm deps

``` shell
npm install
```

** (3) - Write an interface

``` ts
// ...
```


** (4) - Roll it up

``` shell
npm run rollup
```


** (5) - Run the test

``` shell
./main ddd1 # or any example computing assuming 'main.' as prefix and '.qml' as suffix
```

### Usage

``` shell
qmake
make
npm install
npm run rollup
./main ect3 # or any example computing assuming 'main.' as prefix and '.qml' as suffix
```
