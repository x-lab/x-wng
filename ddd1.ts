import * as d3 from 'd3';
import { DDDC } from './dddc';

export class DDD1 extends DDDC {
    graticule: any;
    sphere: any;
    equator: any;
    scale: any;
    rotate: any;
    projection: any;
    path: any;

    n = 480;

    constructor(width: number, height: number) {

        super(width, height);

        this.graticule = d3.geoGraticule10();
        this.sphere = ({type: "Sphere"});
        this.equator = ({type: "LineString", coordinates: [[-180, 0], [-90, 0], [0, 0], [90, 0], [180, 0]]});
        this.scale = d3.interpolate(this.width / 4, (this.width - 2) / (2 * Math.PI));
        this.rotate = d3.interpolate([10, -20], [0, 0]);
        this.projection = this.interpolateProjection(d3.geoOrthographicRaw, d3.geoEquirectangularRaw)
            .scale(this.scale(0))
            .translate([this.width / 2, this.height / 2])
            .rotate(this.rotate(0))
            .precision(0.1);
        this.path = d3.geoPath(this.projection, this.context);
    }

    interpolateProjection(raw0: any, raw1: any): any {
        const mutate = d3.geoProjectionMutator(t => (x, y) => {
            const [x0, y0] = raw0(x, y), [x1, y1] = raw1(x, y);
            return [x0 + t * (x1 - x0), y0 + t * (y1 - y0)];
        });
        let t = 0;
        return Object.assign(mutate(t), {
            alpha(_) {
                return arguments.length ? mutate(t = +_) : t;
            }
        });
    }

    public draw(i: number): void {

        const t = d3.easeCubic(2 * i > this.n ? 2 - 2 * i / this.n : 2 * i / this.n);
        this.projection.alpha(t).rotate(this.rotate(t)).scale(this.scale(t));
        this.context.clearRect(0, 0, this.width, this.height);
        this.context.beginPath();
        this.path(this.graticule);
        this.context.lineWidth = 1;
        this.context.strokeStyle = "#aaa";
        this.context.stroke();
        this.context.beginPath();
        this.path(this.sphere);
        this.context.lineWidth = 1.5;
        this.context.strokeStyle = "#000";
        this.context.stroke();
        this.context.beginPath();
        this.path(this.equator);
        this.context.lineWidth = 1.5;
        this.context.strokeStyle = "#f00";
        this.context.stroke();
    }
}
