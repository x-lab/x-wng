import * as d3 from 'd3';
import { DDDS } from './ddds';

export class DDD4 extends DDDS {

    data: any;

    constructor(width: number, height: number, data: any) {

        super(width, height);

        var value = (x, y) =>
            (1 + (x + y + 1) ** 2 * (19 - 14 * x + 3 * x ** 2 - 14 * y + 6 * x * y + 3 * y ** 2))
            * (30 + (2 * x - 3 * y) ** 2 * (18 - 32 * x + 12 * x * x + 48 * y - 36 * x * y + 27 * y ** 2))

        var x = d3.scaleLinear([-2, 2], [0, width + 28])
        var y = d3.scaleLinear([-2, 1], [height, 0])

        var xAxis = g => g
            .attr("transform", `translate(0,${height})`)
            .call(d3.axisTop(x).ticks(width / height * 10))
            .call(g => g.select(".domain").remove())
            .call(g => g.selectAll(".tick").filter(d => x.domain().includes(d)).remove())

        var yAxis = g => g
            .attr("transform", "translate(-1,0)")
            .call(d3.axisRight(y))
            .call(g => g.select(".domain").remove())
            .call(g => g.selectAll(".tick").filter(d => y.domain().includes(d)).remove())

        var thresholds = d3.range(1, 20).map(i => Math.pow(2, i))

        var color = d3.scaleSequentialLog(d3.extent(thresholds), d3.interpolateMagma)

        var grd = {
            x: null,
            y: null,
            k: null,
            n: null,
            m: null,
        }

        const q = 4; // The level of detail, e.g., sample every 4 pixels in x and y.
        const x0 = -q / 2, x1 = width + 28 + q;
        const y0 = -q / 2, y1 = height + q;
        const n = Math.ceil((x1 - x0) / q);
        const m = Math.ceil((y1 - y0) / q);
        const grid = new Array(n * m);
        for (let j = 0; j < m; ++j) {
            for (let i = 0; i < n; ++i) {
                grid[j * n + i] = value(x.invert(i * q + x0), y.invert(j * q + y0));
            }
        }
        grd.x = -q;
        grd.y = -q;
        grd.k = q;
        grd.n = n;
        grd.m = m;

        var transform = ({type, value, coordinates}) => {
            return {type, value, coordinates: coordinates.map(rings => {
                return rings.map(points => {
                    return points.map(([x, y]) => ([
                        grd.x + grd.k * x,
                        grd.y + grd.k * y
                    ]));
                });
            })};
        }

        var contours = d3.contours()
            .size([grd.n, grd.m])
            .thresholds(thresholds)
        (grid)
            .map(transform)

        this.svg.append("g")
            .attr("fill", "none")
            .attr("stroke", "#fff")
            .attr("stroke-opacity", 0.5)
            .selectAll("path")
            .data(contours)
            .join("path")
            .attr("fill", d => color(d.value))
            .attr("d", d3.geoPath());

        this.svg.append("g")
            .call(xAxis);

        this.svg.append("g")
            .call(yAxis);
    }
}
