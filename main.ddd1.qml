import QtQuick          2.15
import QtQuick.Controls 2.15
import QtQuick.Window   2.15
import QtWebEngine      1.10

ApplicationWindow {

    id: window;

    width: 480;
    height: 280;
    visible: true;

    background: Rectangle {
        color: "#616161";
    }

    WebEngineView {

        id: view;

        anchors.fill: parent;

        backgroundColor: "transparent";

        settings.javascriptEnabled: true
        settings.pluginsEnabled: true
        settings.autoLoadImages: true
        settings.showScrollBars: false;

        property var i: 0;
        property var n: 480;

        url: 'main.html';

        onLoadingChanged: {
            switch(loadRequest.status) {
            case WebEngineLoadRequest.LoadStartedStatus: break;
            case WebEngineLoadRequest.LoadFailedStatus: break;
            case WebEngineLoadRequest.LoadSucceededStatus:

                view.runJavaScript("var instance = X.createDDD1(" + view.width + ", " + view.height + ");");
                view.runJavaScript("instance.draw(0);");

                animation.start();

                break;
            }
        }

        onJavaScriptConsoleMessage: {
            console.log(message, lineNumber, sourceID);
        }

        NumberAnimation on i {

            id: animation;

            running: false;
            loops: Animation.Infinite
            from: 0
            to: view.n
            duration: 3500
            easing.type: Easing.OutQuad;
        }

        onIChanged: view.runJavaScript("instance.draw(" + view.i + ");");
    }
}
